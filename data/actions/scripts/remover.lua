--SCRIPT FEITO POR: POKEMONULTIMATETWO ;P
--
--
--
--
function onUse(cid, item, frompos, item2, topos)

local nick = getPokeballName(item2.uid)
local megas = {
Alakazite = {15131, 1}, -- id da mega stone, quantidade que vai ser adicionada ao player.
CharizarditeX = {15134, 1},
CharizarditeY = {15135, 1},
Blastoisinite = {15133, 1},
Gengarite = {15136, 1},
Pidgeotile = {15791, 1},
Venusaurite = {15793, 1},
Kangaskhanite = {15783, 1},
Aerodactylite = {15786, 1},
Tyranitarite = {15781, 1},
Ampharosite = {15794, 1},
Scizorite = {15784, 1},
Aggronite = {15780, 1},
Blazikenite = {15792, 1},
Mawlite = {15782, 1},
Gardevoirite = {15785, 1},
Absolite = {15787, 1},
Lucarionite = {15788, 1},
Sceptilite = {15789, 1},
Swampertile = {15790, 1},
msgDone = "Sua mega stone foi removida com sucesso.", -- mensagem que vai mandar ao remover a mega stone
msgFail = "Esse pokemon nao segura nenhuma mega stone." -- mensagem que vai mandar se nao tiver a mega stone
}

if #getCreatureSummons(cid) > 0 or isRiderOrFlyOrSurf(cid) then
    doSendMsg(cid, "Retorne seu pokemon para remover a mega Stone.")
    return true
end

if not isPokeball(item2.itemid) then
    return doPlayerSendCancel(cid, MSG_NAO_E_POSSIVEL)
end

if nick == "Aggron" and getItemAttribute(item2.uid, "ehMega") then
doSendMsg(cid, megas.msgDone)
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Aggronite[1], megas.Aggronite[2])

elseif nick == "Alakazam" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Alakazite[1], megas.Alakazite[2])

elseif nick == "Charizard" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem", megaID)
doPlayerAddItem(cid, megas.CharizarditeX[1], megas.CharizarditeX[2])

elseif nick == "Charizard" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem", megaID)
doPlayerAddItem(cid, megas.CharizarditeY[1], megas.CharizarditeY[2])

elseif nick == "Blastoise" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Blastoisinite[1], megas.Blastoisinite[2])

elseif nick == "Gengar" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Gengarite[1], megas.Gengarite[2])

elseif nick == "Pidgeot" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Pidgeotile[1], megas.Pidgeotile[2])

elseif nick == "Venusaur" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Venusaurite[1], megas.Venusaurite[2])

elseif nick == "Kangaskhan" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Kangaskhanite[1], megas.Kangaskhanite[2])

elseif nick == "Aerodactyl" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Aerodactylite[1], megas.Aerodactylite[2])

elseif nick == "Tyranitar" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Tyranitarite[1], megas.Tyranitarite[2])

elseif nick == "Ampharos" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Ampharosite[1], megas.Ampharosite[2])

elseif nick == "Scizor" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Scizorite[1], megas.Scizorite[2])

elseif nick == "Blaziken" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Blaziken[1], megas.Blaziken[2])

elseif nick == "Mawile" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Mawlite[1], megas.Mawlite[2])

elseif nick == "Gardevoir" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Gardevoirite[1], megas.Gardevoirite[2])

elseif nick == "Absol" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Absolite[1], megas.Absolite[2])

elseif nick == "Lucario" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Lucarionite[1], megas.Lucarionite[2])

elseif nick == "Sceptile" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Sceptilite[1], megas.Sceptile[2])

elseif nick == "Swampert" and getItemAttribute(item2.uid, "ehMega") then
doItemEraseAttribute(item2.uid, "ehMega")
doItemEraseAttribute(item2.uid, "yHeldItem")
doPlayerAddItem(cid, megas.Swampertile[1], megas.Swampertile[2])
else
doSendMsg(cid, megas.msgFail)
return true
end
end

-- TUTORIAL DE COMO ADICIONAR NOVOS MEGAS:

--ADICIONE COMO NO EXEMPLO, DEPOIS DO ELSEIF DO WAMPERT:

--elseif nick == "Nome do Pokemon" and getItemAttribute(item2.uid, "ehMega") then
--doItemEraseAttribute(item2.uid, "ehMega")
--doItemEraseAttribute(item2.uid, "yHeldItem")
--doPlayerAddItem(cid, megas.Nome da Mega Stone[1], megas.Nome da Mega Stone[2])

--DEPOIS LA EM CIMA NA TABELA MEGAS = {} VOC�S ADICIONAM O NOME DA MEGA STONE, IGUAL VCS COLOCARAM EMBAIXO, EXEMPLO FEITO:(SEMPRE ADICIONANDO VIRGULA NO PENULTIMO ITEM DA TABELA)

-- local megas = {
-- msgFail = "msg",
-- Sableyite = {17480, 1}
--}

--elseif nick == "Sableye" and getItemAttribute(item2.uid, "ehMega") then
--doItemEraseAttribute(item2.uid, "ehMega")
--doItemEraseAttribute(item2.uid, "yHeldItem")
--doPlayerAddItem(cid, megas.Sableyite[1], megas.Sableyite[2])